<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use App\Models\Comment;
use App\Models\Post;
use Illuminate\Http\Request;

class CommentController extends Controller
{
    /**
     * Handle the incoming request.
     */
    public function __invoke(Request $request, Post $post)
    {
        if (auth()->user()->cannot('user', $post)) {
            return redirect()->route('main.show', $post)
                ->with('warning', 'Please log in to leave a comment.');
        }

        $request->merge([
            'author_id' => auth()->id(),
            'post_id' => $post->id,
        ]);

        $validated = $request->validate([
            'author_id' => 'required',
            'post_id' => 'required',
            'text' => 'required',
        ]);

        Comment::create($validated);

        return redirect()->route('main.show', $post);
    }
}
