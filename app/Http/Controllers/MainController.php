<?php

namespace App\Http\Controllers;

use App\Models\Post;
use Illuminate\Support\Facades\Auth;

class MainController extends Controller
{
    public function index()
    {
        $posts = Post::with(['category', 'author'])->orderBy('created_at', 'DESC')->paginate(3);

        return view('main.index')->with('posts', $posts);
    }

    public function show(Post $post)
    {

        if (Auth::id() && auth()->user()->role === 'admin') {
            return to_route('admin.post.show', $post);
        } elseif (Auth::id() == $post->author_id) {
            return to_route('user.post.show', $post);
        }
        $comments = $post->comments()->with('author')->orderBy('created_at', 'DESC')->paginate(10);

        return view('main.show')->with(['post' => $post, 'comments' => $comments]);
    }
}
