<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Models\Post;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
    /**
     * Handle the incoming request.
     */
    public function __invoke(string $id)
    {
        $categories = Category::all();
        $posts = Post::where('category_id', $id)->orderBy('created_at', 'DESC')->paginate(3);
        $selected_category = $categories->firstWhere('id', $id);
        //        dd($selected_category);
        return view('main.by-categories')->with(['categories' => $categories, 'posts' => $posts, 'selected_category' => $selected_category]);
    }
}
