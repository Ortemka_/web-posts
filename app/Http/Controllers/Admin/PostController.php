<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Category;
use App\Models\Post;
use Illuminate\Http\Request;

class PostController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $posts = auth()->user()->posts()->orderBy('created_at', 'DESC')->paginate(10);

        return view('admin.post.index')->with('posts', $posts);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        $categories = Category::all();

        return view('admin.post.create')->with('categories', $categories);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $request->merge([
            'author_id' => auth()->id(),
        ]);

        $validated = $request->validate([
            'author_id' => 'required',
            'title' => 'required|max:120',
            'category_id' => 'required|not_in:0',
            'text' => 'required',
        ]);

        Post::create($validated);

        return redirect()->route('admin.post.index');
    }

    /**
     * Display the specified resource.
     */
    public function show(Post $post)
    {
        if (auth()->user()->cannot('admin')) {
            return redirect()->route('main.show-post', $post)
                ->with('warning', 'Please log in to leave a comment.');
        }
        $comments = $post->comments()->with('author')->orderBy('created_at', 'DESC')->paginate(10);

        return view('admin.post.show')->with(['post' => $post, 'comments' => $comments]);
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Post $post)
    {
        if (auth()->user()->cannot('admin')) {
            abort(403);
        }

        $categories = Category::all();

        return view('admin.post.edit')->with(['post' => $post, 'categories' => $categories]);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, Post $post)
    {
        if (auth()->user()->cannot('admin')) {
            abort(403);
        }

        $validated = $request->validate([
            'title' => 'required|max:120',
            'category_id' => 'required|not_in:0',
            'text' => 'required',
        ]);

        $post->update($validated);

        return to_route('admin.post.show', $post);
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Post $post) //this will be if in all posts
    {
        if (auth()->user()->cannot('admin')) {
            abort(403);
        }

        $post->delete();

        return redirect()->route('main.page');
    }

    public function destroyPublic(Post $post) // this will be if in user
    {
        if (auth()->user()->cannot('admin')) {
            abort(403);
        }

        $post->delete();

        return redirect()->route('main.page');
    }
}
