<?php

namespace App\Policies;

use App\Models\Post;
use App\Models\User;

class PostPolicy
{
    public function delete(User $user, Post $post)
    {
        return $user->id === $post->author_id;
    }

    public function author(User $user, Post $post)
    {
        return $user->id === $post->author_id;
    }

    public function user(User $user)
    {
        return $user->role === 'user';
    }

    public function admin(User $user)
    {
        return $user->role === 'admin';
    }
}
