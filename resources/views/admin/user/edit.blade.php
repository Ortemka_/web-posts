@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card border border-secondary">
                    <div class="card-header bg-danger-subtle border-danger">{{ __('Edit user') }}</div>

                    <div class="card-body">
                        <form action="{{ route('admin.user.update', $user) }}" method="post">
                            @method('put')
                            @csrf
                            <div class="mb-3 col-sm-6">
                                <label for="name" class="form-label">Title</label>
                                <input name="name" type="text" class="form-control" id="name"
                                       value="{{ @old('name', $user->name) }}">
                                @error('name')
                                <div class="text-danger">Enter name</div>
                                @enderror
                            </div>

                            <div class="mb-3 col-sm-6">
                                <label for="email" class="form-label">Email</label>
                                <input name="email" type="email" class="form-control" id="email"
                                       value="{{ @old('email', $user->email) }}">
                                @error('email')
                                <div class="text-danger">Enter email</div>
                                @enderror
                            </div>

                            <div class="mb-3 ">
                                <select name="role" class="form-select" aria-label="Select user role">
                                    <option value="0">Select user role</option>
                                    <option
                                        value="user" {{ @old('role', $user->role) === 'user' ? "selected" : "" }}>user
                                    </option>
                                    <option
                                        value="admin" {{ @old('role', $user->role) === 'admin' ? "selected" : "" }}>admin
                                    </option>
                                </select>
                                @error('role')
                                <div class="text-danger">Select role</div>
                                @enderror
                            </div>

                            <button type="submit" class="btn btn-outline-success">Submit</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
