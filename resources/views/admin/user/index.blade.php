@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card border border-secondary">
                <div class="card-header bg-danger-subtle border-danger">{{ __('All users') }}</div>

                <div class="card-body">
                    <div class="container py-2">

                        <a href="{{ route('admin.user.create') }}" class="btn btn-outline-secondary">+ Add new user</a>

                        @if(!$users->isEmpty())
                        <div class="my-2 p-2 border-b border-gray-200 w-50 sm:rounded-lg mt-2">
                            <table class="table table-hover mt-2 table table-sm">
                                <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Title</th>
                                    <th>Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($users as $user)
                                    <tr>
                                        <td>{{ $user->id }}</td>
                                        <td>{{ $user->name }}</td>
                                        <td>
                                            <form action="{{ route('admin.user.destroy', $user) }}" method="post">
                                                @csrf
                                                @method('DELETE')
                                                <a href="{{ route('admin.user.show', $user) }}"
                                                   class="me-2 btn btn-outline-success btn-sm">
                                                    See
                                                </a>
                                                <a href="{{ route('admin.user.edit', $user) }}"
                                                   class="me-2 btn btn-outline-primary btn-sm">
                                                    Edit
                                                </a>
                                                <button type="submit" class="btn btn-outline-danger btn-sm"
                                                        onclick=" return confirm('Are you sure you wish to delete this user and all it posts and comments ?')">
                                                    Delete
                                                </button>

                                            </form>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                        {{ $users->links() }}
                        @else
                            <p class="mt-4">No users have been created yet</p>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
@endsection
