@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card border border-success">
                    <div class="card-header bg-success-subtle border-success">{{ __('Create post') }}</div>

                    <div class="card-body">
                        <div class="container py-1">

                            <div class="card-body">
                                <form action="{{ route('admin.post.store') }}" method="post">
                                    @csrf
                                    <div class="mb-3 col-sm-6">
                                        <label for="title" class="form-label">Title</label>
                                        <input name="title" type="text" class="form-control" id="title"
                                               value="{{ @old('title') }}">
                                        @error('title')
                                        <div class="text-danger">Title should be</div>
                                        @enderror
                                    </div>
                                    <div class="mb-3 ">
                                        <select name="category_id" class="form-select" aria-label="Select post Type">
                                            <option value="0">Open this select menu</option>
                                            @foreach($categories as $category)
                                                <option
                                                    value="{{ $category->id }}" {{ $category->id === @old('category_id') ? "selected" : "" }}>
                                                    {{ $category->title }}
                                                </option>
                                            @endforeach
                                        </select>
                                        @error('category_id')
                                        <div class="text-danger">Post type should be</div>
                                        @enderror
                                    </div>
                                    <div class="mb-3 form-floating">
                                        <textarea name="text" class="form-control"
                                                  id="postText">{{ @old('text') }}</textarea>
                                        <label for="postText">Post text</label>
                                        @error('text')
                                        <div class="text-danger">Post type should be</div>
                                        @enderror
                                    </div>
                                    <button type="submit" class="btn btn-success">Submit</button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
@endsection
