@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">{{ __('Edit category') }}</div>

                    <div class="card-body">
                        <form action="{{ route('admin.category.update', $category) }}" method="post">
                            @method('put')
                            @csrf
                            <div class="mb-3 col-sm-6">
                                <label for="title" class="form-label">Title</label>
                                <input name="title" type="text" class="form-control" id="title"
                                       value="{{ @old('title', $category->title) }}">
                                @error('title')
                                <div class="text-danger">Title should be</div>
                                @enderror
                            </div>

                            <button type="submit" class="btn btn-primary">Submit</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
