@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card border border-secondary">
                    <div class="card-header bg-danger-subtle border-danger">{{ __('Create post category') }}</div>

                    <div class="card-body">
                        <form action="{{ route('admin.category.store') }}" method="post">
                            @csrf
                            <div class="mb-3 col-sm-6">
                                <label for="title" class="form-label">Title</label>
                                <input name="title" type="text" class="form-control" id="title">
                                @error('title')
                                <div class="text-danger">Enter title</div>
                                @enderror
                            </div>
                            <button type="submit" class="btn btn-outline-success">Submit</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
