@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card border border-secondary">

                    <div class="card-header bg-danger-subtle border-danger">
                        <a class="fw-bold link-dark text-decoration-none" href="{{ url()->previous() }}">
                            Back</a> &nbsp;/ &nbsp;Category
                    </div>

                    <div class="card-body">
                        <div class="container py-4">

                            <h1 class="text-body-emphasis">{{ $category->title }}</h1>
                            <p>
                                <strong>Posted: </strong> {{ $category->created_at->format('jS M') }}
                            </p>

                            @if(!$posts->isEmpty())
                                <div class="my-2 p-2 border-b border-gray-200 w-50 sm:rounded-lg mt-2">
                                    <table class="table table-hover mt-2 table table-sm">
                                        <thead>
                                        <tr>
                                            <th>ID</th>
                                            <th>Title</th>
                                            <th>Action</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($posts as $post)
                                            <tr>
                                                <td>{{ $post->id }}</td>
                                                <td>{{ $post->title }}</td>
                                                <td>
                                                    <form action="{{ route('admin.post.destroy', $post) }}"
                                                          method="post">
                                                        @csrf
                                                        @method('DELETE')
                                                        <a href="{{ route('admin.post.show', $post) }}"
                                                           class="me-2 btn btn-outline-success btn-sm">
                                                            See
                                                        </a>
                                                        <a href="{{ route('admin.post.edit', $post) }}"
                                                           class="me-2 btn btn-outline-primary btn-sm">
                                                            Edit
                                                        </a>
                                                        <button type="submit" class="btn btn-outline-danger btn-sm"
                                                                onclick=" return confirm('Are you sure you wish to delete this post ?')">
                                                            Delete
                                                        </button>

                                                    </form>
                                                </td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                                {{ $posts->links() }}
                            @else
                                <p class="mt-4">You have not created categories yet</p>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
