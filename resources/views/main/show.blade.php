@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card border border-success">

                    <div class="card-header bg-success-subtle border-success">
                        <a class=" fw-bold link-dark text-decoration-none" href="{{ url()->previous() }}">
                            Back</a> &nbsp;/ &nbsp;User post
                    </div>

                    <div class="card-body">
                        <div class="container py-1">

                            <div class="col d-flex flex-column align-items-start gap-2">
                                <h1 class="text-body-emphasis">{{ $post->title }}</h1>
                            </div>

                        </div>

                        <ul class="list-unstyled text-small ">
                            <li>
                                Author: <strong>{{ $post->author->name }}</strong>,
                                Category: <strong>{{ $post->category->title }}</strong>
                            </li>
                            <li>{{ $post->created_at->diffForHumans() }}</li>
                        </ul>
                        <h5 class="mb-5">{{ $post->text }}</h5>

                        <form action="{{ route('user.comment.store', $post) }}" method="POST">
                            @csrf
                            <div class="w-75">
                                <div class="form-floating">
                                <textarea name="text" class="form-control" placeholder="Leave a comment here"
                                          id="floatingTextarea"></textarea>
                                    <label for="floatingTextarea">Comment text</label>
                                </div>
                            </div>

                            @error('text')
                            <div class="text-danger">Write comment text</div>
                            @enderror

                            @if(auth()->check())
                                <button type="submit" class="btn btn-success mt-2">Leave Comment</button>
                            @else
                                <button type="submit" class="btn btn-success mt-2" disabled>To leave commit login
                                    first
                                </button>
                            @endif
                        </form>

                        @if(!$comments->isEmpty())

                            <div class="my-6 p-6 bg-white border-b border-gray-200 w-75 sm:rounded-lg mt-5">
                                <table class="table table-hover mt-6 table table-sm">
                                    <thead>
                                    <tr>
                                        <th>Author</th>
                                        <th>Comments</th>
                                        <th></th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @if(!empty($comments))
                                        @foreach($comments as $comment)
                                            <tr>
                                                <th scope="col">{{ $comment->author->name }}</th>
                                                <td class="align-middle w-100"
                                                    style="text-align: justify; max-width: 70%">
                                                    {{ $comment->text }}
                                                </td>
                                                <td class="align-middle text-body-secondary">
                                                    <small
                                                        class="text-body-secondary fw-semibold ps-3">{{ $comment->created_at->diffForHumans() }}
                                                    </small>
                                                </td>
                                            </tr>
                                        @endforeach
                                    @endif
                                    </tbody>
                                </table>
                            </div>
                        @else
                            <p class="mt-4">Users have not written comments yet</p>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
