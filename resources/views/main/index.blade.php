@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12 ">
                <div class="card border border-success">
                    <div class="card-header bg-success-subtle border-success">{{ __('All posts') }}</div>

                    <div class="card-body">
                        <div class="container py-4">
                            @if(!$posts->isEmpty())

                                @foreach($posts as $post)
                                    <div class="col-md-10 border border-success border-2 mb-3 rounded-4">
                                        <div class="ms-3 mt-2">
                                            <h2><a class="link-dark text-decoration-none"
                                                   href="{{ route('main.show', $post) }}">{{ $post->title }}</a></h2>
                                            <ul class="list-unstyled text-small">
                                                <li>
                                                    Author: <strong>{{ $post->author->name }}</strong>,
                                                    Category: <strong>{{ $post->category->title }}</strong>
                                                </li>
                                                <li>{{ $post->created_at->diffForHumans() }}</li>
                                            </ul>
                                            <p class="fs-5">{{ Str::limit($post->text, 200) }}</p>

                                            <div class="container text-end">
                                                <a class="btn btn-outline-success mb-2"
                                                   href="{{ route('main.show', $post) }}">READ MORE</a>
                                            </div>
                                        </div>
                                    </div>
                                @endforeach
                                {{ $posts->links() }}
                            @else
                                <p class="mt-4">Other users have not created posts yet. You can be the first</p>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
