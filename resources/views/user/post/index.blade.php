@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card border border-success">
                    <div class="card-header bg-success-subtle border-success">{{ __('My posts') }}</div>

                    <div class="card-body">
                        <div class="container py-4">
                            @forelse($posts as $post)
                                <div class="col-md-10 border border-success border-2 mb-3 rounded-4">
                                    <div class="ms-3 mt-2">
                                        <h2><a class="link-dark text-decoration-none"
                                               href="{{ route('main.show', $post) }}">{{ $post->title }}</a></h2>
                                        <ul class="list-unstyled text-small">
                                            <li>
                                                Author: <strong>{{ $post->author->name }}</strong>,
                                                Category: <strong>{{ $post->category->title }}</strong>
                                            </li>
                                            <li>{{ $post->created_at->diffForHumans() }}</li>
                                        </ul>
                                        <h5>{{ Str::limit($post->text, 200) }}</h5>


                                        <div class="row row-cols-1 row-cols-md-2 align-items-start g-5 py-1">
                                            <div class="col d-flex flex-column align-items-start gap-2">
                                                <form method="post" class=""
                                                      action="{{ route('user.post.destroy', $post) }}">
                                                    @csrf
                                                    @method('DELETE')
                                                    <a href="{{ route('user.post.edit', $post) }}"
                                                       class="btn btn-outline-primary">Edit</a>
                                                    <button type="submit" class="btn btn-outline-danger "
                                                            onclick=" return confirm('Are you sure you wish to delete this post ?')">
                                                        Delete
                                                    </button>
                                                </form>
                                            </div>

                                            <div class="col">
                                                <div class="container text-end">
                                                    <a class="btn btn-outline-success mb-2"
                                                       href="{{ route('main.show', $post) }}">READ MORE</a>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            @empty
                                <p class="mt-4">Other users have not created posts yet. You can be the first</p>
                            @endforelse
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
