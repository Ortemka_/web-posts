<?php

namespace Database\Seeders;

use App\Models\Category;
use Illuminate\Database\Seeder;

class CategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        Category::create([
            'title' => 'Sport',
        ]);
        Category::create([
            'title' => 'Food',
        ]);
        Category::create([
            'title' => 'Games',
        ]);
        Category::create([
            'title' => 'Shop',
        ]);
    }
}
