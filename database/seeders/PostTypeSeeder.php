<?php

namespace Database\Seeders;

use App\Models\PostType;
use Illuminate\Database\Seeder;

class PostTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        PostType::create([
            'title' => 'Sport',
        ]);
        PostType::create([
            'title' => 'Food',
        ]);
        PostType::create([
            'title' => 'Games',
        ]);
        PostType::create([
            'title' => 'Shop',
        ]);
    }
}
