<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Post>
 */
class PostFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        return [
            'author_id' => rand(1, 12),
            'category_id' => rand(1, 4),
            //            'type_id' => rand(1, 4),
            'title' => fake()->title(),
            'text' => fake()->text('200'),
        ];
    }
}
