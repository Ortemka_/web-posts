<?php

use App\Http\Controllers\Admin\CategoryController as AdminCategoryController;
use App\Http\Controllers\Admin\PostController as AdminPostController;
use App\Http\Controllers\Admin\UserController;
use App\Http\Controllers\CategoryController;
use App\Http\Controllers\MainController;
use App\Http\Controllers\User\CommentController;
use App\Http\Controllers\User\PostController as UserPostController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Auth::routes();

Route::get('/', [MainController::class, 'index'])->name('main.page');
Route::get('/post/{post}', [MainController::class, 'show'])->name('main.show');
Route::get('/category/{category}', CategoryController::class)->name('main.category');

// Admin routes
Route::prefix('/admin')->name('admin.')->middleware(['auth', 'role:admin'])->group(function () {
    Route::resource('/post', AdminPostController::class);
    Route::resource('/user', UserController::class);
    Route::resource('/category', AdminCategoryController::class);
});

// User routes
Route::prefix('/user')->name('user.')->middleware(['auth', 'role:user'])->group(function () {
    Route::resource('/post', UserPostController::class);
    Route::post('/comment/store/{post}', CommentController::class)->name('comment.store');
});
